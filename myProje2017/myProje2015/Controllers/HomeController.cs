﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myProje2015.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult About()
        {
            ViewBag.Message = "Your About page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Gallery()
        {
            ViewBag.Message = "Your gallery page.";

            return View();
        }

        public ActionResult Detail()
        {
            ViewBag.Message = "Your ditail page.";

            return View();
        }

        public ActionResult Universities()
        {
            ViewBag.Message = "Your Universities page.";

            return View();
        }

        public ActionResult Admission()
        {
            ViewBag.Message = "Your Admission page.";

            return View();
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Index page.";

            return View();
        }
        public ActionResult _Duyuru()
        {
            ViewBag.Message = "Duyuru page.";

            return View();
        }
        public ActionResult _Slider()
        {
            ViewBag.Message = "Slider page.";

            return View();
        }
        public ActionResult StudentLife()
        {
            ViewBag.Message = "Your StudentLife page.";

            return View();
        }

        public ActionResult uyeGiris()
        {
            ViewBag.Message = "Your uyeGiris page.";

            return View();
        }
    //    [ChildActionOnly]
       

        [HttpPost]
        public ActionResult uyeGirisYardim(string kadi, string password)
        {
            Kullanici uye = new Kullanici();


            


            if (uye != null)
            {
                Session["id"] = uye.kullanici_ID;
                Session["ad"] = uye.kullanici_AdSoyad;

                TempData["mesaj"] = "Hoşgeldiniz";

                return RedirectToAction("AdminHome/Index", "Admin", "");
            }




            TempData["mesaj"] = "Uye Bilgileriniz Yanliş";
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());




        }

        public ActionResult uyeGirisYardim2()
        {
            Session.Clear();

            TempData["mesaj"] = "Uye Bilgileriniz Yanliş";
            return View("Index");
        }



        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            if (Session["lang"] == null)
            {
                Session["lang"] = "en";
            }
            else
            {
                if (Session["lang"].ToString() == "tr")
                {
                    Session["lang"] = "en";
                }
                else
                {
                    Session["lang"] = "tr";
                }
            }

            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

    }
}