﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using myProje2015;

namespace myProje2015.Areas.Admin.Controllers
{
    public class slidersController : Controller
    {
        private myProje2015Entities db = new myProje2015Entities();

        // GET: Admin/sliders
        public async Task<ActionResult> Index()
        {

            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }

            var sliders = db.sliders.Include(s => s.kategoriID);
            return View(await sliders.ToListAsync());
        }

        // GET: Admin/sliders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            slider slider = await db.sliders.FindAsync(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // GET: Admin/sliders/Create
        public ActionResult Create()
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd");
            return View();
        }

        // POST: Admin/sliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "sliderID,slidFoto,slidText,baslangicTarih,bitisTarih,resimYolu,kategoriID")] slider slider)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            if (ModelState.IsValid)
            {
                db.sliders.Add(slider);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", slider.kategoriID);
            return View(slider);
        }

        // GET: Admin/sliders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            slider slider = await db.sliders.FindAsync(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", slider.kategoriID);
            return View(slider);
        }

        // POST: Admin/sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "sliderID,slidFoto,slidText,baslangicTarih,bitisTarih,resimYolu,kategoriID")] slider slider)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            if (ModelState.IsValid)
            {
                db.Entry(slider).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", slider.kategoriID);
            return View(slider);
        }

        // GET: Admin/sliders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            slider slider = await db.sliders.FindAsync(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Admin/sliders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }
            slider slider = await db.sliders.FindAsync(id);
            db.sliders.Remove(slider);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
