﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using myProje2015;

namespace myProje2015.Areas.Admin.Controllers
{
    public class makalesController : Controller
    {
        private myProje2015Entities db = new myProje2015Entities();

        // GET: Admin/makales
        public async Task<ActionResult> Index()
        {
            var makales = db.makales.Include(m => m.kategoriID);
            return View(await makales.ToListAsync());
        }

        // GET: Admin/makales/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            makale makale = await db.makales.FindAsync(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            return View(makale);
        }

        // GET: Admin/makales/Create
        public ActionResult Create()
        {
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd");
            return View();
        }

        // POST: Admin/makales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "makaleID,makaleBaslik,makaleOzet,makaleIcerik,makaleResim,makaleTarih,makaleOkunma,makaleYorumSayisi,kategoriID")] makale makale)
        {
            if (ModelState.IsValid)
            {
                db.makales.Add(makale);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", makale.kategoriID);
            return View(makale);
        }

        // GET: Admin/makales/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            makale makale = await db.makales.FindAsync(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", makale.kategoriID);
            return View(makale);
        }

        // POST: Admin/makales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "makaleID,makaleBaslik,makaleOzet,makaleIcerik,makaleResim,makaleTarih,makaleOkunma,makaleYorumSayisi,kategoriID")] makale makale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(makale).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.kategoriID = new SelectList(db.kategoris, "kategoriID", "kategoriAd", makale.kategoriID);
            return View(makale);
        }

        // GET: Admin/makales/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            makale makale = await db.makales.FindAsync(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            return View(makale);
        }

        // POST: Admin/makales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            makale makale = await db.makales.FindAsync(id);
            db.makales.Remove(makale);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
