﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myProje2015.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        // GET: Admin/AdminHome
        public ActionResult Index()
        {
            if (Session["id"] != null)
            {

            }
            else
            {
                TempData["mesaj"] = "Giriş yetkiniz bulunmamakta";
                return Redirect("/Home/Index");
            }



            return View();
        }


        public ActionResult Cikis()
        {
            Session.Clear();

            TempData["mesaj"] = "Çıkış Yaptınız";
            return Redirect("/Home/Index");
        }
    }
}