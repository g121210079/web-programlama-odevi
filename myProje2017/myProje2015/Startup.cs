﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(myProje2015.Startup))]
namespace myProje2015
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
